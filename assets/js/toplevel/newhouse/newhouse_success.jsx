import React from 'react';
import { localize } from '../../util/localization';

export default class Newhouse_sucess extends React.Component {
  render() {
    return (
      <div>
        <h1 id="sucess-title">{localize('newh_accept_title')}</h1>
        <p>{localize('newh_accept_text') + this.props.location.state.reply_email}</p>
      </div>
    );
  }
}
