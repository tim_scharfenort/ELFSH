import React from 'react';

import { Link } from 'react-router-dom';
import { request } from '../../util/request';
import { urlBase64ToUint8Array } from '../../notifications/util';
import { localize } from '../../util/localization';
import { ValidationError, ValidationMessage } from '../../generic/validationError';
import { ErrorView } from '../../components/error';
import { FullscreenLoading } from '../../components/loading';

export default class Newhouse extends React.Component {
  constructor() {
    super();

    this.state = {
      captcha: null,

      error: null,

      captcha_text: '',
      housename: '',
      username: '',
      password: '',
      password2: '',
      email: '',

      licence1: false,

      captcha_validation: '',
      housename_validation: '',
      username_validation: '',
      password_validation: '',
      password2_validation: '',
      email_validation: '',
      licence_validation: '',
    };
  }

  componentDidMount() {
    document.title = `${localize('page_new')} - ELFSH - ${localize('page_main')}`;

    request(
      '/api/captcha',
    ).then(
      (data) => {
        const blob = new Blob(
          [urlBase64ToUint8Array(data.data)],
        );
        const url = URL.createObjectURL(blob);
        this.setState(
          {
            captcha: url,
            captcha_id: data.id,
          },
        );
      },
    );
  }

  componentWillUnmount() {
    if (this.state.captcha) {
      URL.revokeObjectURL(this.state.captcha);
    }
  }

  validate() {
    let ok = true;

    if (this.state.captcha_text === '') {
      this.setState(
        {
          captcha_validation: localize('newh_captcha_validation'),
        },
      );
      ok = false;
    } else {
      this.setState({
        captcha_validation: '',
      });
    }
    if (this.state.username === '') {
      this.setState(
        {
          username_validation: localize('newh_username_validation'),
        },
      );
      ok = false;
    } else {
      this.setState({
        username_validation: '',
      });
    }
    if (this.state.housename === '') {
      this.setState(
        {
          housename_validation: localize('newh_housename_validation'),
        },
      );
      ok = false;
    } else {
      this.setState({
        housename_validation: '',
      });
    }
    if (this.state.password === '') {
      this.setState(
        {
          password_validation: localize('newh_password_validation'),
        },
      );
      ok = false;
    } else {
      this.setState({
        password_validation: '',
      });
    }
    if (this.state.password2 === '' && this.state.password !== '') {
      this.setState(
        {
          password2_validation: localize('newh_password2_validation'),
        },
      );
      ok = false;
    } else if (this.state.password2 !== this.state.password) {
      this.setState(
        {
          password2_validation: localize('newh_password2_validation_nomatch'),
        },
      );
      ok = false;
    } else {
      this.setState(
        {
          password2_validation: '',
        },
      );
    }

    if (this.state.email.indexOf('@') === -1) {
      this.setState(
        {
          email_validation: localize('newh_email_validation'),
        },
      );
      ok = false;
    } else {
      this.setState({
        email_validation: '',
      });
    }

    if (!this.state.licence1) {
      this.setState(
        {
          licence_validation: localize(
            'newh_licence_validation',
          ),
        },
      );
      ok = false;
    } else {
      this.setState(
        {
          licence_validation: '',
        },
      );
    }
    return ok;
  }

  submit() {
    if (!this.validate()) {
      return;
    }
    this.setState(
      { error: undefined },
    );
    request(
      '/api/new_house',
      {
        captcha_id: this.state.captcha_id,
        name: this.state.housename,
        captcha: this.state.captcha_text,
        userinfo: {
          username: this.state.username,
          password: this.state.password,
          email: this.state.email,
        },
      },
    ).then(
      (data) => this.props.history.push(
        `/${this.props.match.params.lang}/new/done`,
        {
          reply_email: data.reply_email,
        },
      ),
    ).catch(
      (err) => {
        window.scrollTo(0, 0);
        this.setState(
          { error: err },
        );
      },
    );
  }

  render() {
    if (this.state.captcha === null) {
      return <FullscreenLoading />;
    }

    return (
      <div>
        <ErrorView error={this.state.error} />
        <h1>{localize('newh_welcome')}</h1>
        <p>{localize('newh_captcha_title')}</p>
        <img src={this.state.captcha} alt="captcha" />
        <ValidationMessage error={this.state.captcha_validation} />
        <label className="inputLabel">
          {localize('newh_captcha_label')}
          <input
            value={this.state.captcha_text}
            onChange={
          (ev) => this.setState({ captcha_text: ev.target.value })
}
            id="in-captcha"
          />
        </label>
        <h2>{localize('newh_basic_title')}</h2>

        <ValidationMessage error={this.state.housename_validation} />
        <label className="inputLabel">
          {localize('newh_house_name')}
          <input
            value={this.state.housename}
            onChange={
            (ev) => this.setState(
              { housename: ev.target.value },
            )
          }
            id="in-house-name"
          />
        </label>
        <p>{localize('newh_house_name_description')}</p>

        <h2>{localize('newh_personal_title')}</h2>
        <ValidationMessage error={this.state.username_validation} />
        <label className="inputLabel">
          {localize('login_username')}
          <input
            value={this.state.username}
            onChange={
                 (ev) => this.setState(
                   { username: ev.target.value },
                 )
               }
            id="in-username"
          />
        </label>
        <p>{localize('newh_username_description')}</p>

        <ValidationMessage error={this.state.password_validation} />
        <label className="inputLabel">
          {localize('login_password')}
          <input
            type="password"
            value={this.state.password}
            onChange={(ev) => this.setState({
              password: ev.target.value,
            })}
            id="in-password"
          />
        </label>
        <p>{localize('newh_password_description')}</p>

        <ValidationMessage error={this.state.password2_validation} />
        <label className="inputLabel">
          {localize('login_password_confirm')}
          <input
            type="password"
            value={this.state.password2}
            onChange={(ev) => this.setState({
              password2: ev.target.value,
            })}
            id="in-password-2"
          />
        </label>

        <ValidationMessage error={this.state.email_validation} />
        <label className="inputLabel">
          {localize('settings_user_email')}
          <input
            type="email"
            value={this.state.email}
            onChange={(ev) => this.setState({
              email: ev.target.value,
            })}
            id="in-user-email"
          />
        </label>
        <p>{localize('newh_user_email_description')}</p>

        <h2>{localize('newh_licence_title')}</h2>

        <ValidationMessage error={this.state.licence_validation} />
        <label className="inputLabel">
          <input
            type="checkbox"
            checked={this.state.licence1}
            onChange={
          (event) => this.setState({ licence1: event.target.checked })
}
            id="licence-1"
          />
          {localize('newh_licence_1')}
          <a href="/static/licence.txt">{localize('newh_licence_2')}</a>
        </label>

        <button
          onClick={this.submit.bind(this)}
          id="submit"
        >
          {localize('cost_submit')}
        </button>
        <br />
        <Link to={`${this.props.match.params.lang}/login`}>{localize('back')}</Link>
      </div>
    );
  }
}
