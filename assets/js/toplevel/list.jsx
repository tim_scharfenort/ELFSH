import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { formatDate } from '../util/date';
import { formatMoney, localize } from '../util/localization';
import { Nav } from '../components/nav';
import { Modal } from '../generic/modal';
import { request } from '../util/request';
import { ErrorView } from '../components/error';
import { FullscreenLoading } from '../components/loading';
import { RefreshDetector } from '../components/refreshDetector';

class TableElement extends React.PureComponent {
  constructor() {
    super();

    this.state = {
      hover: false,
    };
  }

  render() {
    const { elem, onChange } = this.props;
    const { hover } = this.state;

    return (
      <>
        <span
          onClick={
                  () => {
                    this.props.onChange({ state: (elem.state + 1) % 3, extra: elem.extra });
                  }
                }
          className={`table-elem table-elem-${elem.state}`}
        >
          {elem.state === 0 ? 'x' : elem.state === 1 ? 'e'
            : elem.state === 2 ? 'k' : '?'}
        </span>

        {
          elem.extra >= 1
            ? (
              <span
                className="table-elem table-elem-delete"
                onClick={() => onChange(
                  { state: elem.state, extra: elem.extra - 1 },
                )}
                onMouseEnter={() => this.setState({ hover: true })}
                onMouseLeave={() => this.setState({ hover: false })}
                role="button"
                tabIndex={0}
              >
                +
                {hover ? elem.extra - 1 : elem.extra}
              </span>
            )
            : undefined

        }

        {
          this.props.elem.state === 0 ? undefined
            : (
              <span
                className="table-elem table-elem-add"
                onClick={() => onChange({
                  state: elem.state,
                  extra: elem.extra + 1,
                })}
              >
                +
              </span>
            )
        }
      </>
    );
  }
}

class TableRow extends React.PureComponent {
  render() {
    const { users, day, date } = this.props;

    return (
      <tr>
        <td>{formatDate(date)}</td>
        {
        users.map(
          (i) => (
            <td key={i.id}>
              <TableElement
                user={i}
                elem={this.props.value[i.id]}
                onChange={(value) => {
                  this.props.onChange(i.id, day, date, value);
                }}
              />
            </td>
          ),
        )
      }

      </tr>
    );
  }
}

// Programming today is a race between software engineers striving to build bigger and better
// idiot-proof programs, and the universe trying to produce bigger and better idiots. So far, the
// universe is winning. - Rick Cook

export default class EatList extends React.Component {
  constructor() {
    super();
    this.state = {
      data: undefined,
      error: undefined,
      loading: true,
      modal: false,
    };

    this.buttonClickEventHandler = this.buttonClickEventHandler.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.refresh = this.refresh.bind(this);
  }

  componentDidMount() {
    document.title = `${localize('page_list')} - ELFSH`;

    this.refresh();
  }

  buttonClickEventHandler(person, day, date, value) {
    const { data } = this.state;
    const d = new Date();
    const h = Number(data.house_info.closing_time.substr(0, 2));
    const m = Number(data.house_info.closing_time.substr(3, 2));
    if (value.state === 1 && day === 0
      && (d.getHours() > h || (d.getHours() === h && d.getMinutes() > m))) {
      this.setState(
        {
          modal: true,
          click_info: {
            person,
            day,
            date,
            value,
          },
        },
      );
    } else {
      this.sendRequest(person, day, date, value);
    }
  }

  sendRequest(person, day, date, value) {
    request(
      `/api/update/${this.props.match.params.house_id}/${person}/${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
      {
        state: value.state,
        extra: value.extra,
      },
    ).catch(
      (error) => {
        this.setState({ error });
      },
    );

    this.setState(
      (state) => {
        const t = state.data;
        t.table[day][person] = value;
        return { data: t };
      },
    );
  }

  refresh() {
    const { match } = this.props;

    this.setState(
      { loading: true },
    );
    request(`/api/future_days/${match.params.house_id}/`).then(
      (r) => {
        this.setState(
          {
            data: r,
            loading: false,
          },
        );
      },
    ).catch(
      (reason) => {
        console.error(reason);
        this.setState({
          error: reason,
          loading: false,
        });
      },
    );
  }

  render() {
    return (
      <>
        <Nav
          match={this.props.match}
          selected="lijst"
        />

        <RefreshDetector onRefresh={this.refresh} />

        <ErrorView error={this.state.error} />

        {
        this.state.loading
          ? <FullscreenLoading /> : undefined
      }

        {
        this.state.data ? (
          <div className="table-container">
            <table cellSpacing="0">
              <tbody>
                <tr className="table-heading">
                  <th />
                  {
                this.state.data.users.map(
                  (f) => (
                    <th key={f.id} className={f.is_me ? '' : 'th-disabled'}>
                      <div className="table-person-name">{`${f.name}`}</div>
                    </th>
                  ),
                )
              }
                </tr>
                <tr className="table-heading">
                  <th>{localize('list_money')}</th>
                  {
                this.state.data.users.map(
                  (f) => (
                    <th
                      key={f.id}
                      className={f.is_me ? '' : 'th-disabled'}
                    >
                      {formatMoney(f.value)}
                    </th>
                  ),
                )
              }
                </tr>
                <tr className="table-heading">
                  <th>
                    {localize('list_point')}
                    {' '}
                    <FontAwesomeIcon icon="utensils" />
                  </th>
                  {
                this.state.data.users.map(
                  (f) => (
                    <th
                      key={f.id}
                      className={f.is_me ? '' : 'th-disabled'}
                    >
                      {`${f.points}`}
                    </th>
                  ),
                )
              }
                </tr>
                {this.state.data.table.map(
                  (f, i) => (
                    <TableRow
                      value={f}
                      date={new Date(new Date(this.state.data.date) - (-1000 * 60 * 60 * 24 * i))}
                      day={i}
                      users={this.state.data.users}
                      key={i}
                      onChange={this.buttonClickEventHandler}
                    />
                  ),
                )}
              </tbody>
            </table>
          </div>
        ) : undefined

      }
        {
        this.state.modal ? (
          <Modal>
            <p>
              {localize('list_confirm')}
            </p>
            <button
              onClick={
            () => {
              this.sendRequest(
                this.state.click_info.person,
                this.state.click_info.day,
                this.state.click_info.date,
                this.state.click_info.value,
              );
              this.setState({ modal: false });
            }
          }
              type="button"
            >
              {localize('list_confirm_ok')}
            </button>
            <button
              onClick={() => this.setState({ modal: false })}
              type="button"
            >
              {localize('list_confirm_cancel')}
            </button>
          </Modal>
        ) : undefined
      }
      </>
    );
  }
}
