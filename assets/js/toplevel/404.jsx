import React from 'react';
import { Link } from 'react-router-dom';
import { localize } from '../util/localization';

export default class ErrorPage extends React.PureComponent {
  componentDidMount() {
    document.title = `404 - ELFSH - ${localize('page_main')}`;
  }

  render() {
    return (
      <>
        <h1>{localize('404_title')}</h1>

        <p className="text-container">
          {localize('404_prelink')}
        </p>

        <img src="/static/images/wireframe.svg" style={{ width: '40%' }} alt="" />
        <br />
        <Link to="/" className="button">{localize('404_link')}</Link>
        <div className={'spacing'} />
        <p className="text-container">
          {localize('404_postlink')}
        </p>
      </>
    );
  }
}
