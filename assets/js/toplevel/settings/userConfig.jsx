import React from 'react';
import { localize } from '../../util/localization';
import { formatDate } from '../../util/date';

export class UserConfig extends React.PureComponent {
  constructor() {
    super();

    this.onDelete = this.onDelete.bind(this);
  }

  onDelete() {
    this.props.showModal(this.props.user.id, this.props.user.name);
  }

  render() {
    return (
      <div className="settings-user-info">
        <div>
          <div className="settings-user-info-label">{localize('settings_user_name')}</div>
          <div className="settings-user-info-value">{this.props.user.name}</div>
        </div>
        <div>
          <div className="settings-user-info-label">{localize('settings_user_email')}</div>
          <div className="settings-user-info-value">{this.props.user.email}</div>
        </div>
        <div>
          <div className="settings-user-info-label">{localize('settings_user_lastlogin')}</div>
          <div className="settings-user-info-value">
            {this.props.user.last_login === null
              ? localize('setting_never')
              : formatDate(new Date(this.props.user.last_login))}
          </div>
        </div>
        <div>
          <div className="settings-user-info-label">{localize('settings_user_remove')}</div>
          <div>
            <button
              type="button"
              onClick={this.onDelete}
            >
              {localize('settings_user_remove')}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
