import { set_cookie } from './cookies.js';

const localization = {
  // errors
  err404: {
    en: '404 - page not found',
    nl: '404 - pagina niet gevonden',
  },
  error_invalid_response: {
    en: 'Invalid server response',
    nl: 'Bericht van server niet begrepen',
  },
  loading: {
    en: 'Loading...',
    nl: 'Laden...',
  },
  back: {
    en: 'Back',
    nl: 'Terug',
  },
  error_not_authenticated_1: {
    en: 'Authentication failed, please ',
    nl: 'Authenticatie mislukt, probeer opnieuw ',
  },
  error_not_authenticated_2: {
    en: 'log in',
    nl: 'in te loggen',
  },
  error_not_authenticated_3: {
    en: ' again',
    nl: '',
  },
  // page names
  page_main: {
    nl: 'De eetlijst en kostenbeheer site',
    en: 'The food and money management system',
  },
  page_houses: {
    en: 'Groups',
    nl: 'Groepen',
  },
  page_invitation: {
    en: 'Invitation',
    nl: 'Uitnodiging',
  },
  page_costs: {
    en: 'costs',
    nl: 'kosten',
  },
  page_list: {
    en: 'list',
    nl: 'lijst',
  },
  page_login: {
    en: 'login',
    nl: 'inloggen',
  },
  page_new: {
    en: 'create account',
    nl: 'account aanmaken',
  },
  page_reset: {
    en: 'reset password',
    nl: 'wacthwoord vergeten',
  },
  page_settings: {
    en: 'settings',
    nl: 'instellingen',
  },
  page_verify: {
    en: 'verify',
    nl: 'verifieren',
  },
  // dates
  date_weekday: {
    en: [
      'Sun',
      'Mon',
      'Tue',
      'Wen',
      'Thu',
      'Fri',
      'Sat',
    ],
    nl: [
      'zo',
      'ma',
      'di',
      'wo',
      'do',
      'vr',
      'za',
    ],
  },
  // voor header
  header_welcome: {
    en: 'Welcome, ',
    nl: 'Welkom, ',
  },
  header_notify: {
    en: 'Enable notifications',
    nl: 'Notificaties toestaan',
  },
  header_logout: {
    en: 'Log out',
    nl: 'Uitloggen',
  },
  // voor footer
  footer_line_1: {
    en: 'ELFSH - Maurits van Riezen. Please report bugs.',
    nl: 'ELFSH - Maurits van Riezen. Bugs of andere fouten graag melden.',
  },
  footer_line_2: {
    en: 'Last Changed: ',
    nl: 'Laatste Update: ',
  },
  footer_line_3: {
    en: 'View source and contribute at ', // extra space is important
    nl: 'Bekijk bron en draag een steentje bij op ',
  },
  // For navigation
  nav_list: {
    en: 'List',
    nl: 'Lijst',
  },
  nav_cost: {
    en: 'Costs',
    nl: 'Kosten',
  },
  nav_settings: {
    en: 'Settings',
    nl: 'Instellingen',
  },
  // For error page
  '404_title': {
    en: 'This page cannot be found',
    nl: 'Deze pagina bestaat niet',
  },
  '404_prelink': {
    en: 'Oops, it seems like you stumbled upon a page that doesn\'t exist. Click the link below or'
      + ' your back button to get back on solid ground.',
    nl: 'Oops, het lijkt erop alsof je op een pagina die niet bestaat bent beland. Klik op de link'
      + 'hieronder of op de terug knop van je browser om weer op vaste grond te komen.',
  },
  '404_link': {
    en: 'Back to homepage',
    nl: 'Terug naar hoofdpagina',
  },
  '404_postlink': {
    en: 'Did you arrive here from a link inside ELFSH? If so, please send me a email so I can fix it.'
      + ' My email is \x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c'
      + ' or create a issue on gitlab.',
    nl: 'Ben je via een interne link bij deze pagina beland? Ik zou het erg waarderen als je even contact'
      + 'opneemt om dit te melden, via mijn email'
      + ' (\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c)'
      + 'of via een issue op gitlab.',
  },
  // for login screen
  login_welcome: {
    en: 'Log in on ELFSH',
    nl: 'Log in op ELFSH',
  },
  login_page_welcome: {
    en: 'ELFSH',
    nl: 'ELFSH',
  },
  login_page_subtitle: {
    en: 'Eating together made easier',
    nl: 'Eetlijst en Kostenbeheer',
  },
  login_username: {
    en: 'username',
    nl: 'gebruikersnaam',
  },
  login_password: {
    en: 'password',
    nl: 'wachtwoord',
  },
  login_password_confirm: {
    en: 'Confirm password',
    nl: 'Bevestig wachtwoord',
  },
  login_start: {
    en: 'Start now',
    nl: 'Meteen beginnen',
  },
  login_create_account: {
    en: 'Not a member yet? Create an account',
    nl: 'Nog geen lid? Maak een account aan',
  },
  login_forgot_password: {
    en: 'Forgot password?',
    nl: 'Wachtwoord vergeten?',
  },
  login_login: {
    en: 'Sign in!',
    nl: 'Inloggen',
  },
  screenshot_list: {
    en: 'Set yourself as "eating" in ELFSH',
    nl: 'De eetlijst van ELFSH',
  },
  screenshot_cost: {
    en: 'Share bills evenly easier with the cost management system',
    nl: 'Kosten eerlijk delen met het kosten management systeeem',
  },
  screenshot_user: {
    en: 'Add as many people as needed with the ELFSH user management system',
    nl: 'Beheer mensen makkelijk in het ELFSH gebruiker beheer systeem',
  },
  // The ELFSH marketing description
  promo_p_1: {
    en:
      `ELFSH is a system to make eating together easier. It allows you to sign up to eat or cook on a given day.
    ELFSH is intended for shared accomodation or other groups who want easier coordination.`,
    nl: 'ELFSH is een gratis eetlijst app en costen verdeler, een alternatief voor eetlijst.nl, turff of wiebetaaltwat.nl.',
  },
  promo_how: {
    en: 'How ELFSH works',
    nl: 'Hoe ELFSH eetlijst werkt',
  },
  promo_header_list: {
    en: 'Keep track of who cooks and who eats',
    nl: 'Eetlijst: Houd bij wie er meeëet',
  },
  promo_p_list: {
    en: 'Easily set yourself as cooking or eating on a given day',
    nl: 'Zet jezelf gemakkelijk op eten of koken in de eetlijst',
  },
  promo_header_point: {
    en: 'Optional "Cooking Points" ensure everyone cooks equally often',
    nl: 'Gelijke kansen om te koken met kookpunten',
  },
  promo_p_point: {
    en: 'Distribute the cooking task fairly with the cooking point system. Spend points to eat, gain points to cook',
    nl: 'Eerlijk de kooktaak delen met kookpunten. Geef ze uit om te eten, spaar ze door te koken',
  },
  promo_header_cost: {
    en: 'Fairly distribute shared costs',
    nl: 'Kostenbeheer: Deel gezamelijke kosten eerlijk',
  },
  promo_p_cost: {
    en: 'ELFSH includes a cost-splitting functionality, similar to SplitWise. Using this, you can split the costs'
      + ' for food, but also for arbitrary other expenses.',
    nl: 'Verdeel de kosten voor dingen die je "op het huis" koopt, of als je een keer een brood voor iemand haalt, net als wie betaalt wat',
  },
  promo_header_cedit: {
    en: 'Extra features for cost submission',
    nl: 'Handige functies voor costen invullen',
  },
  promo_p_cedit: {
    en: 'Other usefull features include: having some people pay different amounts, removing people'
      + ' from expenses later, or negative amounts to represent income.',
    nl: 'Ondersteunt onder andere sommige mensen meer laten betalen, acteraf bewerken'
      + 'of zelfs mensen verwijderen, of negatieve bedragen (om inkomsten te representeren).',
  },
  promo_header_6: {
    en: 'Manage users and invite new people',
    nl: 'Beheer gebruikers en nodig mensen uit',
  },
  promo_p_7: {
    en: 'Easily add new people to your group via email. A group can have as many people as you like,'
      + 'while one person can also be member of any number of groups. ',
    nl: 'Nieuwe mensen kun je eenvoudig aan je groep toevoegen met email'
      + ' Een persoon kan lid zijn van meerdere groepen, en een groep kan zoveel leden hebben als je wilt.',
  },
  promo_header_8: {
    en: 'Interested? Try it right now',
    nl: 'Interesse? Probeer het nu!',
  },
  promo_compare: {
    en: 'How does ELFSH compare to other options',
    nl: 'Hoe is ELFSH beter dan de alternatieven?',
  },
  compare_time: {
    en: 'Speed',
    nl: 'Efficiëncie',
  },
  compare_time_desc: {
    en: 'Time to set yourself as eating for a given day',
    nl: 'Tijd nodig om jezelf op eten te zetten',
  },
  compare_click: {
    en: 'Clicks',
    nl: 'Kliks',
  },
  compare_second: {
    en: 'Seconds',
    nl: 'Seconden',
  },
  compare_yes: {
    en: 'Yes',
    nl: 'Ja',
  },
  compare_no: {
    en: 'No',
    nl: 'Nee',
  },
  compare_security: {
    en: 'Private Accounts',
    nl: 'Apparte Accounts',
  },
  compare_account_pp: {
    en: 'Yes',
    nl: 'Ja',
  },
  compare_account_all: {
    en: 'Shared account for whole house',
    nl: 'Gedeeld account',
  },
  compare_account_nopassword: {
    en: 'Password cannot be changed',
    nl: 'Wachtwoord kan niet verandert worden',
  },
  compare_encryption: {
    en: 'Encryption',
    nl: 'Beveiligde Verbinding',
  },
  compare_encryption_detail: {
    en: 'Strength of TLS encryption according to SSLLabs.com',
    nl: 'Beveiliging kracht volgen SSLLabs.com',
  },
  compare_encryption_none: {
    en: 'No secure connection possible',
    nl: 'Geen beveiligde verbinding mogelijk',
  },

  // Voor reset scherm
  reset_title: {
    en: 'Reset Password',
    nl: 'Wachtwoord Resetten',
  },
  reset_explain: {
    en: 'Forgotten your password? Enter your email and a password reset link will be sent to your email. This link '
      + 'will stay valid a limited amount of time.',
    nl: 'Je wachtwoord vergeten? Voer je email adres in en je ontvangt een mail om je wachtwoord te resetten. '
      + 'Deze link is beperkte tijd geldig.',
  },
  // Voor kosten scherm
  cost_submit: { // also used in settings
    en: 'Submit!',
    nl: 'Toepassen',
  },
  cost_one_to_all: {
    en: 'Everyone x1',
    nl: 'Iedereen x1',
  },
  cost_cancel: {
    en: 'Cancel',
    nl: 'Annuleer',
  },
  cost_description: {
    en: 'Description: ',
    nl: 'Beschrijving: ',
  },
  cost_amount: {
    en: 'Amount: ',
    nl: 'Gemaakte kosten: ',
  },
  cost_paid_by: {
    en: 'Paid by: ',
    nl: 'Betaald door: ',
  },
  cost_payed_for: {
    en: 'Participants: ',
    nl: 'Deelnemers: ',
  },
  cost_deleted: {
    en: '[Deleted]',
    nl: '[Verwijderd]',
  },
  cost_add_expense: {
    en: 'Add expense',
    nl: 'Kosten toevoegen',
  },
  cost_more_options: {
    en: 'More options',
    nl: 'Meer opties',
  },
  cost_is_meal: {
    en: 'Cooking points',
    nl: 'Kookpunten',
  },
  // For list
  list_money: {
    en: 'Money',
    nl: 'Geld',
  },
  list_point: {
    en: 'Cooking points',
    nl: 'Kookpunten',
  },
  list_confirm: {
    en: 'The closing time has passed. While it is still possible to sign in, please confirm with whoever is'
      + 'cooking to make sure there is enough food for you.',
    nl: 'De sluitingstijd is verstreken. Je kunt je nog inschrijven, maar het is mischien handig om even te '
      + 'overleggen met de kok of er genoeg eten zal zijn.',
  },
  list_confirm_ok: {
    en: 'Confirm',
    nl: 'Bevestig',
  },
  list_confirm_cancel: {
    en: 'Cancel',
    nl: 'Annuleer',
  },
  // For settings
  settings_title: {
    en: 'Settings for ',
    nl: 'Settings for ',
  },
  settings_general: {
    en: 'General',
    nl: 'Algemeen',
  },
  settings_name: {
    en: 'House name',
    nl: 'Naam huis',
  },
  settings_name_description: {
    en: 'A nice house deserves a good name',
    nl: 'Een goed huis verdient een goede naam',
  },
  settings_endtime: {
    en: 'Closing time: ',
    nl: 'Sluitingstijd: ',
  },
  settings_endtime_description: {
    en: 'After this time you will get a warning if you sign up to eat',
    nl: 'Na deze tijd krijg je een waarschuwing als je nog probeert mee te eten',
  },
  settings_dayseditable: {
    en: 'Editable time: ',
    nl: 'Dagen bewerkbaar: ',
  },
  settings_dayseditable_description: {
    en: 'This is how long expenses can be edited after creation',
    nl: 'Zo lang kunnen kosten nog bewerkt worden',
  },
  settings_timezone: {
    en: 'Timezone: ',
    nl: 'Tijd zone:',
  },
  settings_timezone_description: {
    en: 'The timezone used to decide the end of the day and the closing time',
    nl: 'De tijdzone word gebruikt voor het beslissen wanneer een dag eindigt of de slutingstijd geld',
  },
  settings_users: {
    en: 'Users',
    nl: 'Gebruikers',
  },
  settings_extra_options: {
    en: 'Extra Options',
    nl: 'Geavanceerde Opties',
  },
  settings_export_data: {
    en: 'Export Expenses',
    nl: 'Uitgaven Exporteren',
  },
  settings_invitations: {
    en: 'Invite people to join the house',
    nl: 'Uitnodigingen',
  },
  settings_user_name: {
    en: 'Name: ',
    nl: 'Naam: ',
  },
  settings_user_email: {
    en: 'Email: ',
    nl: 'Email: ',
  },
  settings_user_lastlogin: {
    en: 'Last log-in',
    nl: 'Laatst ingelogd',
  },
  settings_user_remove: {
    en: 'Remove',
    nl: 'Verwijder',
  },
  settings_user_send: {
    en: 'Send invitation',
    nl: 'Verstuur uitnodiging',
  },
  setting_user_language: {
    en: 'Language: ',
    nl: 'Taal: ',
  },
  setting_lang_dutch: {
    en: 'Dutch',
    nl: 'Nederlands',
  },
  setting_lang_english: {
    en: 'English',
    nl: 'Engels',
  },
  setting_never: {
    en: 'Never',
    nl: 'Nooit',
  },

  // Settings for invitation
  invitation_match_1: {
    en: 'You must login to the account matching the email \'',
    nl: 'Om deze uitnodiging te accepteren, moet je inloggen met het emailadres waarom je deze uitnodiging ontvangen heeft',
  },
  invitation_match_2: {
    en: ' in order to accept this invitation',
    nl: '',
  },

  invitation_logout_title: {
    en: 'This invitation is not accessible from this account',
    nl: 'Deze uitnodiging is niet bechikbaar voor deze account',
  },
  invitation_logout_1: {
    en: 'This invitation is linked to the email\'',
    nl: 'Deze uitnodiging is gelinkt aan het emailadres \'',
  },
  invitation_logout_2: {
    en: '\'. This email is already associated with an account, thus only'
      + 'that account can accept this invitation',
    nl: '\'. Alleen deze account mag deze uitnodiging accepteren',
  },
  invitation_logout_3: {
    en: 'You may accept this invitation by signing out of this account, and'
      + 'into the correct account. It is also possible to reset the account password'
      + 'if you do not remember it.',
    nl: 'Je kunt deze uitnodiging accepteren door bij deze account uit te loggen'
      + 'en bij de andere account in te loggen.',
  },
  invitation_logout: {
    en: 'Sign out',
    nl: 'Uitloggen',
  },
  invitation_confirm_welcome: {
    en: 'Welcome back to ELFSH',
    nl: 'Welkom terug bij ELFSH',
  },
  invitation_confirm_1: {
    en: 'You are about to join \'',
    nl: 'Je staat op het punt je aan te sluiten bij \'',
  },
  invitation_confirm_2: {
    en: '\'. Confirm?',
    nl: '\'. Bevestigen?',
  },
  invitation_confirm: {
    en: 'Confirm',
    nl: 'Bevestigen',
  },
  invitation_done_title: {
    en: 'All set!',
    nl: 'Alles geregeld!',
  },
  invitation_done_link: {
    en: 'Continue',
    nl: 'Doorgaan',
  },
  invitation_invalid_page: {
    en: 'Page not found: ',
    nl: 'Pagina niet gevonden:  ',
  },

  err_internal: {
    en: 'An internal error occured',
    nl: 'Een interne fout is opgetreden',
  },

  err_explain: {
    en: 'Sorry for the inconveniance. Please report the error to error to '
      + '\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c'
      + '. Please include the technical details displayed below, as well as your browser, operating system,'
      + ' and anything else unusual about your setup.',
    nl: 'Onze excuses. Zou je deze fout willen melden naar '
      + '\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x40\x6d\x6f\x75\x73\x65\x74\x61\x69\x6c\x2e\x6e\x6c'
      + '. Vermeld in je email ook de technische details hieronder, en welke browser en besturingsysteem je '
      + 'gebuikt.',
  },

  err_details: {
    en: 'Technical details',
    nl: 'Technishe details',
  },

  // For new houes
  newh_welcome: {
    en: 'Welcome',
    nl: 'Welkom',
  },
  newh_captcha_title: {
    en: 'First, please verify you are human:',
    nl: 'Eerst, even bewijzen dat je een mens bent:',
  },
  newh_captcha_label: {
    en: 'Characters above:',
    nl: 'de karakters hier boven:',
  },
  newh_basic_title: {
    en: 'Basic settings',
    nl: 'Basis Instellingen',
  },
  newh_house_name: {
    en: 'House name',
    nl: 'Naam huis',
  },
  newh_house_name_description: {
    en: 'The name of your house, not your own name. You can change this later.',
    nl: 'De naam van je huis, niet je eingen naam. Deze kan je later aanpassen',
  },
  newh_personal_title: {
    en: 'Personal Settings',
    nl: 'Gebruikers Instellingen',
  },
  newh_username_description: {
    en: 'Your own name, not the name of the house. Watch out: This can\'t easily be changed later',
    nl: 'Vul je eigen naam in, niet die van het huis. Let up: Deze kan niet makkelijk worden veranderd',
  },
  newh_licence_title: {
    en: 'Licence Agreement',
    nl: 'Voorwaarden',
  },
  newh_password_description: {
    en: 'Your own password, other users will have the chance to create their own password',
    nl: 'Je eigen wachtwoord, je huisgenoten krijgen de kans om hun eigen wachtwoord te kiezen',
  },
  newh_user_email_description: {
    en: 'We will never send you unnececairy emails',
    nl: 'We zullen nooit onnodige emails versturen',
  },
  newh_licence_1: {
    en: 'I Accept the terms of the ',
    nl: 'Ik ga akkoord met de ',
  },
  newh_licence_2: {
    en: 'terms and conditions',
    nl: 'Voorwaarden',
  },
  newh_captcha_validation: {
    en: 'Please fill in the captcha',
    nl: 'Vul the CAPTCHA in',
  },
  newh_username_validation: {
    en: 'Please enter a username',
    nl: 'Vul een gebruikersnaam in',
  },
  newh_housename_validation: {
    en: 'Please enter a name for your house/group',
    nl: 'Vul een naam in voor jouw huis/groep',
  },
  newh_password_validation: {
    en: 'Please enter a password',
    nl: 'Vul een wachtwoord in',
  },
  newh_password2_validation: {
    en: 'Please confirm your password',
    nl: 'Vul uw wachtwoord twee keer in',
  },
  newh_password2_validation_nomatch: {
    en: 'Passwords do not match',
    nl: 'Wachtwoorden komen niet overeen',
  },
  newh_email_validation: {
    en: 'Please enter a valid email address',
    nl: 'Vul een geldig email address in',
  },
  newh_licence_validation: {
    en: 'You must accept the licence agreement',
    nl: 'U moet akkoord gaan met de voorwaarden',
  },
  newh_accept_title: {
    en: 'Welcome to ELFSH',
    nl: 'Welkom bij ELFSH',
  },
  newh_accept_text: {
    en: 'A email has been sent containing a confirmation link to finalize creating your account? Did not get an email?'
      + ' Check your spam folder. The email will be sent from ',
    nl: 'Een email is verstuurd met een link om je account te activeren. Geen email gekregen? Check je spam map. De'
      + ' email zal verstuurd worden vanaf ',
  },
};

export function setLanguage(lang) {
  set_cookie('lang', lang);
  const l = document.getElementsByTagName('html')[0];
  l.lang = lang;
  localStorage.setItem('lang', lang);
}

export function getLanguage() {
  return localStorage.getItem('lang');
}

export function localize(name) {
  const language = localStorage.getItem('lang');
  return localization[name] === undefined ? `[Error translating ${name}]` : localization[name][language];
}

export function formatMoney(amount) {
  const n = (`${Number(amount) / 100}`).split('.');
  if (n.length === 1) {
    return `€${n[0]}.-`;
  }
  if (n[1].length === 2) {
    return `€${n[0]}.${n[1]}`;
  }
  return `€${n[0]}.${n[1]}0`;
}

export function getDefaultLanguage() {
  return ((navigator.language || navigator.userLanguage) === 'nl' ? 'nl' : 'en');
}
