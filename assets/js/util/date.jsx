import { localize } from './localization.js';

export function formatDate(date) {
  // console.log(date);

  return `${localize('date_weekday')[date.getDay()]
  } ${date.getDate()}/${
    date.getMonth() + 1}/${
    date.getFullYear()}`;
}
