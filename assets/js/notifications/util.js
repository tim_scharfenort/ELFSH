import {request} from '../util/request';

export function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4)
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/')

    var rawData = window.atob(base64)
    var outputArray = new Uint8Array(rawData.length)

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i)
    }
    return outputArray;
}

export function loadVersionBrowser(userAgent) {
    var ua = userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name: 'IE', version: (tem[1] || '')};
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/);
        if (tem != null) {
            return {name: 'Opera', version: tem[1]};
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return {
        name: M[0],
        version: M[1]
    };
}

const applicationServerKey = "BPykqHKEm3vjlftIG0deXu9wt5hHx5Fn4hPHq_7L4n_Ya7Gp-0GuggIaRWzJR6zYlmaCN5lczWKLfOwpyGiXOCE";

export function enablePushNotifications() {
    console.log("enabling push notifications...");
    if (!'serviceWorker' in navigator) {
        console.log("Push notifications not supported");
        return;
    }
    navigator.serviceWorker.ready.then(
        (registration) => {
            return registration.pushManager.subscribe({
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(applicationServerKey)
            });
        }
    ).then(
        (sub) => {
            console.log("subscription", sub);
            let endpointParts = sub.endpoint.split("/");
            let registration_id = endpointParts[endpointParts.length - 1];
            let data = {
                'browser': loadVersionBrowser(navigator.userAgent).name.toUpperCase(),
                'p256dh': btoa(String.fromCharCode.apply(null, new Uint8Array(sub.getKey('p256dh')))),
                'auth': btoa(String.fromCharCode.apply(null, new Uint8Array(sub.getKey('auth')))),
                'name': 'ELFSH',
                'registration_id': registration_id,
                'endpoint': sub.endpoint,
                'info': sub.toJSON()
            };
            return request(
                "/api/subscribe",
                data
            )
        }
    ).catch(
        (error) => {
            console.error(error);
            return Promise.reject(error)
        }
    )
}