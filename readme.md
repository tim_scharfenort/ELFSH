# About
## What is ELFSH

[ELFSH  can be found here.](https://elfsh.mousetail.nl) 

![icon](/static/images/eat_ill.svg)

Are you tired of messaging people every day about who wants to arange food? Would you rather
not send payment requests for every communual expense when you know you'll probably have to pay
another one the next day? Do you also feel like green and white make a great color combination?
Then ELFSH is the app for you!

## Keep track of who cooks and who eats

With on tap on your phone, everybody knows you want food tonight. The cookingpoint system makes
it easy to decide who will be responsible. On only 10 seconds you can notify your housemates who will
be cooking. Want to bring guests? Easily add another person and the costs and points will be distributed
fairly and automatically.

## Keep track of expenses

Bought toilet paper for the house? Put it on EFLSH in a few minutes. ELFSH will keep track of the
balance of every member, so you can either pay everything bulk sum, or just buy some extra stuff when
your balance becomes too low.

## Manage users and invite new people

Easily add new people to your house via email. A house can have as many people as you like,
while a person can also be a member of multiple houses. 

# Running in debug mode

Notice a problem you would like to fix? You can clone the source and run
it locally. Just submit a pull request when it's done.

## Basic settings

Create a file `passwords.py` under `/ELFSH`. The contents of this file should look like this:

    EMAIL_HOST_PASSWORD = (email password)
    SECRET_KEY = (application key)
    POSTGRES_PASSWORD = (password to postgres database)
    HOST = (host name, for use in links in emails)

If the POSTGRES_PASSWORD is ommited, a sqlite database will be used.

## Setting up node

Ensure you are using at least version 10.14.0 of node and version 6.4.1 of npm.

Run either `npm install`, or `npm ci`. Then run `npm run debug` to build the debug javascript bundle. You will
get a few warnings, there will probably be fixed eventually.

## Setting up python

Create a virtual env: `python3 -m virtualenv myvenv` on linux, or `py -3 -m virtualenv myvenv` on windows.

Enter it: `. venv\b\activate` or `venv\bin\activate.bat`.

Install dependencies: `pip install -r requirements.txt`

## Initialize the database

`python manage.py migrate`

## Ready to go!

`python manage.py runserver`

# Running in production mode

Don't trust my sysadmin skills? You can run it on your own server if you like.
The basic way the app is structured on the official server is as follows:

![arcitecture](static/images/promo/server_arcitecture.png)

There are more ways this can be structured, but this way seems to work fine.

To make this process easier, there is a provided `deploy.py` that automatically runs the commands to get the dependencies
and build static files etc. However, you will need to manually configure a webserver to serve files.

## Setup NGINX

If you have NGINX installed, go to `etc/nginx/conf.d` and make a new file `elfsh.conf`. It should contain, at a minimum,
the following:

    upstream django {
        server unix:/// {install location} /elfsh.sock
    }
    
    server {
        listen 443 ssl http2;
        server_name {your server name}
        charset utf-8;
        
        ssl_certificate {ssl certificate location};
        ssl_certificate {ssl certificate key location};
        ssl_dhparam {ssl diffie-helman key location};
        
        location /static {
            alias {install location}/static-root;
        }
        
        location / {
            uwsgi_pass django;
            include {install location}/uwsgi_params;
        }
    }

## Configure UWSGI

Create a file `uwsgi_params` under the project root directory. It should contain the following:

    uwsgi_param  QUERY_STRING       $query_string;
    uwsgi_param  REQUEST_METHOD     $request_method;
    uwsgi_param  CONTENT_TYPE       $content_type;
    uwsgi_param  CONTENT_LENGTH     $content_length;
    
    uwsgi_param  REQUEST_URI        $request_uri;
    uwsgi_param  PATH_INFO          $document_uri;
    uwsgi_param  DOCUMENT_ROOT      $document_root;
    uwsgi_param  SERVER_PROTOCOL    $server_protocol;
    uwsgi_param  REQUEST_SCHEME     $scheme;
    uwsgi_param  HTTPS              $https if_not_empty;
    
    uwsgi_param  REMOTE_ADDR        $remote_addr;
    uwsgi_param  REMOTE_PORT        $remote_port;
    uwsgi_param  SERVER_PORT        $server_port;
    uwsgi_param  SERVER_NAME        $server_name;

Also, create a file `elfsh_uwsgi.ini` in the same location:

    [uwsgi]
    
    chdir = { install location }
    module = ELFSH.wsgi
    home = { install location } /myvenv
    master = true
    processes = 10
    socket = { install location }/elfsh.sock
    vacuum = true
    chmod-socket = 664

Now, we want to set up uwsgi in emperor mode. In order to do this run the following:

    # create a directory for the vassals
    sudo mkdir /etc/uwsgi
    sudo mkdir /etc/uwsgi/vassals
    # symlink from the default config directory to your config file
    sudo ln -s {install location} elfsh_uwsgi.ini /etc/uwsgi/vassals/
    
Now, we want to setup UWSGI to run as a service under systemctl. So create a file `emperor.uwsgi.service` under
`/etc/systemctl/system/multi-user.target.wants` with the following contents:

    [Unit]
    Description=uWSGI Emperor
    After=syslog.target
    
    [Service]
    ExecStart=/usr/local/bin/uwsgi --emperor /etc/uwsgi/vassals --uid nginx --gid nginx --logto /var/log/uwsgi-emperor.log
    RuntimeDirectory=uwsgi
    KillSignal=SIGQUIT
    Type=notify
    StandardError=syslog
    NotifyAccess=all
    Restart=always
    [Install]
    WantedBy=multi-user.target
    
Now restart everyting:

    systemctl restart nginx.service
    systemctl restart emperor.uwsgi.service
    
Now, assuming the database is setup, everything should work.
