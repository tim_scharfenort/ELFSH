from .frontend_test import FrontendTest
from ELFSH_MAIN.models import CaptchaRecord
from django.core import mail
import time


class TestCreateHouseFrontend(FrontendTest):
    def testNewHouse(self):
        CaptchaRecord.objects.all().delete()

        self.client.logout()
        self.waitForElement('#new-link').click()

        self.waitForElement('#in-captcha')

        captcha = CaptchaRecord.objects.get()

        self.browser.find_element_by_id('in-captcha').send_keys(
            captcha.value
        )

        self.browser.find_element_by_id('in-house-name').send_keys(
            'test-house-name'
        )

        self.browser.find_element_by_id('in-username').send_keys(
            'test-user'
        )

        self.browser.find_element_by_id('in-password').send_keys(
            '1e4B$n52Bc'
        )

        self.browser.find_element_by_id('in-password-2').send_keys(
            '1e4B$n52Bc'
        )

        self.browser.find_element_by_id('in-user-email').send_keys(
            'testuser@elfsh.mousetail.nl'
        )

        self.browser.find_element_by_id('licence-1').click()

        self.browser.find_element_by_id('submit').click()

        self.waitForElement(
            '#sucess-title'
        )

        self.assertEqual(len(mail.outbox), 1)
        message = mail.outbox[0]
        link = max(
            message.body.split(),
            key=lambda line: len(line.split('/'))
        )

        link = '/'.join(link.split('://')[1].split('/')[1:])
        link = self.live_server_url + '/' + link

        self.browser.get(link)
        self.waitForElement('div.settings-user-info-container')
        # TODO: Fix bug where logout will not work when navigating directly to the url
        self.waitForElement("#logout-button").click()
        self.waitForElement('#password')
        self.waitForElement('#username').send_keys('test-user')
        self.browser.find_element_by_id('password').send_keys('1e4B$n52Bc')
        self.browser.find_element_by_tag_name('button').click()
        self.waitForElement(
            'table'
        )
