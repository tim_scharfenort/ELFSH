from .test_views import BasicTestCase
import datetime
import json
from ELFSH_MAIN import models


class TestEatState(BasicTestCase):
    def check(self, house=None, user=None, day=None, month=None, year=None, state=0, extra=0,
              expected_return_value=200):
        date = datetime.date.today()

        if house is None:
            house = self.house

        if user is None:
            user = self.users[0]

        if day is None:
            day = date.day

        if month is None:
            month = date.month

        if year is None:
            year = date.year

        result = self.client.post('/api/update/{}/{}/{}/{}/{}'
                                  .format(house.id,
                                          user.id,
                                          day,
                                          month,
                                          year),
                                  json.dumps(
                                      {
                                          "state": state,
                                          "extra": extra
                                      }
                                  ),
                                  content_type='application/json')

        self.assertEqual(result.status_code, expected_return_value)

    def testValid1(self):
        self.check(state=0, extra=0, expected_return_value=200)

    def testValid2(self):
        self.check(user=self.users[2], state=1, extra=0, expected_return_value=200)

        states = models.EatState.objects.all()
        self.assertEqual(len(states), 1)

        state = states[0]
        self.assertEqual(state.user, self.users[2])

    def testInvalid1(self):
        self.check(state=4, expected_return_value=400)

    def testInvalid2(self):
        self.check(state=-1, expected_return_value=400)

    def testInvalid3(self):
        self.check(state=1, extra=-1, expected_return_value=400)

    def testNotLoggedIn(self):
        self.client.logout()

        self.check(expected_return_value=403)

    def testLoggedInWrong(self):
        user = self.users[0]
        membership = models.HouseMembership.objects.get(user=user, house=self.house)
        membership.delete()

        self.check(expected_return_value=403)

