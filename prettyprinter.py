from __future__ import print_function

import threading
import collections
import time
import sys
import os
import traceback
import textwrap

ESC = b"\x1b["

SPINNER = "|/-\\"

STATUS_PENDING = 'pending'
STATUS_DONE = 'done'
STATUS_FAILED = 'failed'

STATUS_MESSAGE = 'message'
STATUS_WARNING = 'warning'
STATUS_ERROR = 'error'

SUCCESS_CHAR = b"v"
FAIL_CHAR = b"x"

COLOR_YELLOW = ESC + b"33" + b"m"
COLOR_GREEN = ESC + b"32m"
COLOR_RED = ESC + b"31m"
COLOR_CLEAR = ESC + b"0" + b"m"
COLOR_BOLD = ESC + b"1" + b"m"


def output(t):
    sys.stdout.buffer.write(t)


class _Pretty():
    def __init__(self, que):
        self.que = que
        self.running = True
        self.statuses = []
        os.system("")  # Somehow prevents bugs

        # self.animatedThings = []
        self.frameNum = 0

    def _moveUp(self, amount):
        output(ESC + str(amount).encode("ASCII") + b"A")

    def _moveDown(self, amount):
        output(ESC + str(amount).encode("ASCII") + b"B")

    def _moveVertically(self, amount):
        if (amount > 0):
            self._moveDown(amount)
        elif (amount < 0):
            self._moveUp(-amount)

    def _goToColumn(self, column):
        output(ESC + str(column).encode("ASCII") + b"G")

    @staticmethod
    def run(que):
        p = _Pretty(que)
        p.mainloop()

    def mainloop(self):
        while self.running:
            while len(self.que):
                self.process_command(self.que.popleft())
            time.sleep(0.25)
            self.update()
            sys.stdout.flush()

    def process_command(self, command):
        name = command[0]
        args = command[1:]
        if (name == "quit"):
            self.running = False
        elif (name == "status"):
            self.add_status(*args)
        elif (name == "changestatus"):
            self.change_status(*args)
        pass

    def update(self):
        self.frameNum += 1

        y = 0
        for index, i in enumerate(self.statuses):
            if (i[1] == STATUS_PENDING):
                newy = -len(self.statuses) + index

                self._moveVertically(newy - y)
                self._goToColumn(3)
                output(COLOR_YELLOW +
                       SPINNER[self.frameNum % len(SPINNER)].encode("ASCII") +
                       COLOR_CLEAR)
                y = newy

        self._moveVertically(-y)
        self._goToColumn(0)
        print("B", end="")

    def status_text(self, status):
        if (status == STATUS_PENDING):
            return (COLOR_YELLOW + b"[ | ] " + COLOR_CLEAR + COLOR_BOLD,
                    COLOR_CLEAR)
        elif (status == STATUS_DONE):
            return (COLOR_GREEN + b"[ " + SUCCESS_CHAR + b" ] " + COLOR_BOLD + COLOR_CLEAR,
                    COLOR_CLEAR)
        elif (status == STATUS_FAILED):
            return (COLOR_RED + b"[ " + FAIL_CHAR + b" ] " + COLOR_BOLD + COLOR_CLEAR,
                    COLOR_CLEAR)
        elif (status == STATUS_MESSAGE):
            return (COLOR_CLEAR + b'        ', b'')  # two tabs
        elif (status == STATUS_WARNING):
            return (COLOR_YELLOW + b'        ', COLOR_CLEAR)
        elif (status == STATUS_ERROR):
            return (COLOR_RED + b'        ', COLOR_CLEAR)

    def add_status(self, name, status):
        self.statuses.append((name, status))
        self._goToColumn(0)
        prefix, postfix = self.status_text(status)
        output(prefix + name.encode("ASCII") + postfix + b"\n")

        # self.animatedThings.append([[0,0], "spinner"])
        # for i in self.animatedThings:
        #    i[0][1]-=1

    def change_status(self, status):
        index = 0
        for index, i in enumerate(reversed(self.statuses)):
            if i[1] in (STATUS_PENDING, STATUS_DONE, STATUS_FAILED):
                break

        index = len(self.statuses) - index - 1
        offset = len(self.statuses) - index

        self.statuses[index] = (self.statuses[index][0], status)
        self._moveVertically(-offset)
        self._goToColumn(0)
        output(self.status_text(status)[0])
        self._moveVertically(offset)


class Pretty():
    def __init__(self):
        self.que = collections.deque()

        self.thread = threading.Thread(target=_Pretty.run, args=(self.que,))

    def kill(self):
        self.que.appendleft(("quit",))

    def add_status(self, name, state=STATUS_PENDING):
        self.que.append(("status", name.replace('\t', '    '), state))

    def status(self, name):
        self.add_status(name)

    def change_status(self, state):
        self.que.append(("changestatus", state))

    def log_traceback(self):
        fmt = ''.join(traceback.format_exception(*sys.exc_info())).split('\n')
        for line in fmt:
            for subline in textwrap.wrap(line, 80):
                self.error(subline)

    def error(self, message):
        self.add_status(message, STATUS_ERROR)

    def message(self, message):
        self.add_status(message, STATUS_MESSAGE)

    def log_dict(self, dct):
        for k in dct.keys():
            self.message("[" + str(k) + "]" + repr(dct[k]))

    def warning(self, message):
        self.add_status(message, STATUS_WARNING)

    def done(self):
        self.change_status(STATUS_DONE)

    def failed(self):
        self.change_status(STATUS_FAILED)

    def __enter__(self):
        self.thread.start()
        return self

    def __exit__(self, *args):
        self.kill()


if __name__ == "__main__":

    print(sys.stdout.isatty())
    # payload = b'\n<ESC> [47m \x1b[47mWhite\x1b[0m (white)\n'
    with Pretty() as p:
        p.add_status("hello")
        time.sleep(1)
        p.change_status(STATUS_DONE)
        p.add_status("goodbye")
        time.sleep(1)
        p.add_status("Log message", STATUS_MESSAGE)
        p.add_status("Log waring", STATUS_WARNING)
        p.add_status("Log error", STATUS_ERROR)
        try:
            raise ValueError("help")
        except Exception as ex:
            p.log_traceback()
        time.sleep(1)
        p.change_status(STATUS_FAILED)
        p.add_status("whohoo", STATUS_DONE)
        pass

    # os.system("echo "+payload.decode("ASCII"))
