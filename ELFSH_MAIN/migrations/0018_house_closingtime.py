# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-09-17 07:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ELFSH_MAIN', '0017_expensepayedfor_extra'),
    ]

    operations = [
        migrations.AddField(
            model_name='house',
            name='closingTime',
            field=models.TextField(default='16:00', max_length=6),
            preserve_default=False,
        ),
    ]
