# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-08-29 13:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ELFSH_MAIN', '0014_expense_is_meal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expense',
            name='date',
            field=models.DateField(),
        ),
    ]
