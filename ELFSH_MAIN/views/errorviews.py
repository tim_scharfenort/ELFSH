from django.http.response import (HttpResponseBadRequest,
                                  HttpResponseForbidden,
                                  HttpResponseNotFound, HttpResponseServerError)
from webpack_loader_splitting.utils import get_as_tags

from ELFSH_MAIN.util.localization import localize


def generic_error(request, message):
    if request.path.startswith("/api"):
        return message
    else:
        m_replaced = str(message).replace("\n", "<br>")

        return f"""
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title</title>
    {''.join(get_as_tags('style', 'css'))}
</head>
<body>
<div id="container" class="main-content">
    <h1>Oops</h1>
    <div class="text-container">
        <div class="errorMessage">{m_replaced}</div>
    </div>
    <img src="/static/images/wireframe.svg" style="width:40%">
    </img>
    <br>
    <a class="button" href="/">Back/Terug</a>
</div>
</div>
</body>
</html>
"""


def handler400(request, exception):
    return HttpResponseBadRequest(generic_error(request, localize(request, "error_400").format(str(exception))))


def handler403(request, exception):
    return HttpResponseForbidden(generic_error(request, localize(request, "error_403").format(str(exception))))


def handler_csrf(request, reason):
    return HttpResponseBadRequest(generic_error(request, localize(request, "error_csrf").format(str(reason))))


def handler404(request, exception):
    return HttpResponseNotFound(generic_error(request, localize(request, "error_404").format(str(exception))))


def handler500(request):
    return HttpResponseServerError(generic_error(request, localize(request, "error_500")))


def return_404(request):
    return HttpResponseNotFound(generic_error(request, localize(request, "error_404").format("")))
