from django.http import HttpResponse, HttpResponsePermanentRedirect

import webpack_loader_splitting.utils as webpack_loader


def serviceworker(request):
    with open(webpack_loader.get_files('serviceworker')[0]["path"]) as f:
        content = f.read()

    return HttpResponse(
        content,
        content_type="application/javascript",
    )


def robots(request):
    with open("static/robots.txt") as f:
        content = f.read()

    return HttpResponse(content,
                        content_type="text/plain")


def favicon(request):
    return HttpResponsePermanentRedirect(redirect_to="/static/images/icon.png")
