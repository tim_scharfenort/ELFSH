import smtplib
import uuid

from django.http import (HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseBadRequest, HttpResponseServerError,
                         JsonResponse)
from django.shortcuts import get_object_or_404
from django.contrib.auth import models as authmodels
from django.contrib.auth import login
from django.contrib.auth.password_validation import validate_password, ValidationError
from django.core.mail import send_mail
from django.conf import settings
from django.views.decorators.http import require_POST, require_safe

from ELFSH_MAIN import models
from ELFSH_MAIN.util.localization import localize
from ELFSH_MAIN.util.decorators import login_decorators, validation
from ELFSH_MAIN.schemas import invitation_schema


@require_POST
@login_decorators.require_housemembership
@validation.validate(
    invitation_schema.send_invitation_schema
)
def send_invitation(request, house, data):
    try:
        # If a inactive user with that email exists, simply add them again
        membership = models.HouseMembership.objects.get(house=house, user__user__email=data["email"])
        membership.active = True
        membership.save()

        return JsonResponse({"status": "success", "message": "re-activated user"})
    except models.HouseMembership.DoesNotExist:
        pass

    invitation = models.Invitation(name=data["name"], house=house, email=data["email"].lower())
    invitation.save()

    try:
        send_mail(
            localize(request, "email_invite_subject", lang=data["lang"]),
            localize(request, "email_invite_content", lang=data["lang"]).format(
                repr(data["name"])[1:-1],  # Escape, but remove quotes
                repr(house.name),
                settings.HOST + "/" + request.COOKIES.get('lang', 'en')[:2] + "/invitation/" + str(invitation.id)
            ),
            settings.EMAIL_HOST_USER,
            [data["email"]],
            fail_silently=False
        )
    except smtplib.SMTPException as ex:
        return HttpResponseServerError("Failed to send email: ", *ex.args)

    return JsonResponse({"status": "success"})


@require_POST
@validation.validate(invitation_schema.verify_invitation_schema)
def verify_invitation(request, data):
    try:
        invitation = models.Invitation.objects.get(id=uuid.UUID(hex=data["uuid"]))
    except models.Invitation.DoesNotExist:
        return HttpResponseBadRequest("No Invitation with this ID found, it may have expired.")

    user_exists = True
    try:
        authmodels.User.objects.get(email=invitation.email)  # emails should be lower case
    except authmodels.User.DoesNotExist:
        user_exists = False

    if request.user.is_authenticated:
        user_logged_in = True
        if request.user.email == invitation.email:
            user_same = True
        else:
            user_same = False
    else:
        user_logged_in = False
        user_same = False

    return JsonResponse(
        {
            "user_exists": user_exists,
            "user_loggedin": user_logged_in,
            "user_same": user_same,
            "email": invitation.email,

            "house_name": invitation.house.name
        }
    )


@require_POST
@validation.validate(
    invitation_schema.register_schema
)
def register(request, data):  # Sign up

    if data["password"] != data["password2"]:
        return HttpResponseBadRequest("Passwords do not mach")

    try:
        authmodels.User.objects.get(username=data["username"])
        return HttpResponseBadRequest("Username not unique")
    except authmodels.User.DoesNotExist:
        pass

    try:
        invitation = models.Invitation.objects.get(id=data["uuid"])
    except models.Invitation.DoesNotExist:
        return HttpResponseBadRequest("Invitation with that ID not found")

    user = authmodels.User(
        username=data["username"].strip(),
        email=invitation.email,
    )

    try:
        validate_password(data["password"], user)
    except ValidationError as ex:
        return HttpResponseBadRequest(*ex.args)

    user.set_password(data["password"])

    user.save()  # only save is the password is valid
    login(request, user=user)

    elfsh_user = models.ELFSHUser(
        user=user
    )
    elfsh_user.save()

    house_membership = models.HouseMembership(
        user=elfsh_user,
        house=invitation.house
    )
    house_membership.save()

    invitation.delete()

    return JsonResponse(
        {"status": "success",
         "user_id": elfsh_user.id}
    )


@require_POST
@login_decorators.require_login
@validation.validate(
    invitation_schema.accept_invitation_schema
)
def accept_invitation(request, data):
    try:
        invitation = models.Invitation.objects.get(id=data["uuid"])
    except models.Invitation.DoesNotExist:
        return HttpResponseBadRequest("Invitation does not exist")

    try:
        user = authmodels.User.objects.get(email=invitation.email)
        if user != request.user:
            return HttpResponseBadRequest("This email belongs to another account")
    except authmodels.User.DoesNotExist:
        pass  # All good, no conflicting user exists
    elfsh_user = models.ELFSHUser.objects.get(user=request.user)

    try:
        models.HouseMembership.objects.get(
            user=elfsh_user,
            house=invitation.house
        )
        return HttpResponseBadRequest("You are already a member of this house")
    except models.HouseMembership.DoesNotExist:
        membership = models.HouseMembership(
            user=elfsh_user,
            house=invitation.house
        )
    membership.save()

    invitation.delete()

    return JsonResponse({"status": "success"})
