from ELFSH_MAIN import models


def get_user_info(request, house, extended=False):
    memberships = models.HouseMembership.objects.filter(house=house, active=True)

    info = []
    for membership in memberships:
        dat = {
            "name": membership.user.user.username,
            "id": membership.user.user.id,
            "value": membership.money,
            "points": membership.points,
            "is_me": membership.user.user.id == request.user.id
        }

        if extended:
            dat["email"] = membership.user.user.email
            dat["last_login"] = (membership.user.user.last_login.date().isoformat() if
                                 membership.user.user.last_login is not None else
                                 None)

        info.append(dat)

    info.sort(
        key=lambda x: x["name"]
    )

    info.sort(
        key=lambda x: -x["is_me"]
    )

    return info


def get_house_info(request, house: models.House):
    return {
        "id": house.id,
        "name": house.name,
        "closing_time": house.closingTime,
        "editable_days": house.editableDays,
        "timezone": house.timeZone
    }


def get_account_info(request, house):
    if request.user.is_authenticated:
        return {
            "id": request.user.id
        }
    else:
        return {
            "authenticated": False
        }
