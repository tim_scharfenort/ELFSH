from . import *

set_house_info_schema = DictSchema(
    {
        "name": StringSchema(),
        "closing_time": StringSchema(regex="\d\d:\d\d", message="validation_closing_time"),
        "editable_days": IntSchema(),
        "timezone": StringSchema()
    }
)

add_cost_schema = DictSchema(
    {
        "description": StringSchema(regex=".+"),
        "amount": IntSchema(),
        "payed_for": DictSchema(
            required_params={}
        ),
        "paid_by": IntSchema(),
        "id": IntSchema(),
        "deleted": BoolSchema(),
        "is_meal": BoolSchema()
    },
    {
        "max": IntSchema()
    }
)

get_costs_schema = DictSchema(
    required_params={},
    optional_params={
        "max": IntSchema()
    }
)

insert_day_schema = DictSchema(
    required_params={
        "state": IntSchema(min=0, max=3, message="list_bad_state"),
        "extra": IntSchema(min=0, max=10_000_000, message="list_bad_extra")
    }
)
