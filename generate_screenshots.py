from selenium.webdriver import Firefox, FirefoxOptions


def generate_screenshots():
    """
    Doesn't currently work, will try to implement as a test
    :return: None
    """
    options = FirefoxOptions()
    # options.headless = True
    browser = Firefox(
        options=options
    )
    browser.implicitly_wait(10)
    browser.close()


if __name__ == "__main__":
    generate_screenshots()
